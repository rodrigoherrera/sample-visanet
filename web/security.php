<?php

define ('HMAC_SHA256', 'sha256');
define ('SECRET_KEY', '4bd1d3d756ef44a8ad2bc28ed890b41dd5635d38d429422f892b542c21d42cff3809fc31dec34310b79e9bbd275103b4a9b9f5dc480e4981a80046ab88d6a9834d92cb66f88c413ba1581f3154f0171fa2d32648f8a145188013fa9965ece4b28d7c3949eca84cee9ca5439454605623d660142be1344d7a9a76a7b52f58adf4');

//8a06bc05dbac34df9b008269c31e8c98

function sign ($params) {
  return signData(buildDataToSign($params), SECRET_KEY);
}

function signData($data, $secretKey) {
    return base64_encode(hash_hmac('sha256', $data, $secretKey, true));
}

function buildDataToSign($params) {
        $signedFieldNames = explode(",",$params["signed_field_names"]);
        foreach ($signedFieldNames as $field) {
           $dataToSign[] = $field . "=" . $params[$field];
        }
        return commaSeparate($dataToSign);
}

function commaSeparate ($dataToSign) {
    return implode(",",$dataToSign);
}

?>
