<?php 
//DB Accesses
error_reporting(E_ALL); // E_ALL or 0
ini_set('display_errors', 1); // 1 or 0

$dbHost = "tangle-production-db.c3nnppoweqhg.us-west-1.rds.amazonaws.com";
$dbUser = "development";
$dbPass = "somepassword";
$dbName = "";

$mysqli = new mysqli($dbHost, $dbUser, $dbPass, "");
if ($mysqli->connect_errno) {
    echo "Failed to connect to MySQL: " . $mysqli->connect_error;
}

$mysqli->set_charset("utf8");
$mysqli->query("INSERT INTO `tangle_main_db`.`dev_webhooks` (`id_dev_webhook`, `data`, `created_at`) VALUES (NULL, '". json_encode($_POST)."', CURRENT_TIMESTAMP)");

header('Content-Type: application/json');
echo json_encode($_POST);