<?php
$request     = (object)Input::all();
        $firstName   = $request->nombre;
        $lastName    = $request->apellido;
        $email       = str_replace(' ', '', strtolower($request->nombre . $request->apellido)) . '@guate.net';

        $vencimiento = explode('/', $request->vencimiento);
        $mes         = $vencimiento[0];
        $ano         = $vencimiento[1];
        $ipaddress   = Request::getClientIp();
        $numero      = uniqid();
        $cvv         = $request->cvv;
        $direccion   = $request->direccion;
        $pais        = $request->pais;
        $zipcode     = $request->zipcode;
        $finger      = $request->deviceFingerprintID;

        $client = new cybsController();

        $trans = [];

        $trans['deviceFingerprintID']      = $finger;

        $trans['ccAuthService_run']        = 'true';
        $trans['ccCaptureService_run']     = 'true';
        $trans['merchantReferenceCode']    = $numero;
        $trans['billTo_firstName']         = $firstName;
        $trans['billTo_lastName']          = $lastName;
        $trans['billTo_ipAddress']         = $ipaddress;
        $trans['billTo_street1']           = $direccion;
        $trans['billTo_country']           = $pais;
        $trans['billTo_city']              = $pais;
        $trans['billTo_postalCode']        = $zipcode;
        $trans['billTo_email']             = $email;

        $trans['card_accountNumber']      = $request->tarjeta;
        $trans['card_expirationMonth']    = $mes;
        $trans['card_expirationYear']     = $ano;
        $trans['card_cvNumber']           = $cvv;
        $trans['purchaseTotals_currency'] = 'GTQ';
        $trans['item_0_unitPrice']        = $request->monto;
        $trans['item_0_productSKU']       = uniqid();
        $trans['item_0_productName']      = 'Nombre del producto';

        dd($trans);
        $reply = $client->runTransaction($trans);

        $response = parse_ini_string($reply);
        $aprobado = false;
        $autorizacion = '';

        switch ($response['decision']) {
            case 'REJECT':
                echo('Murio la transaccion');
                break;
            case 'ACCEPT':
                $aprobado = true;
                echo('Paso la transaccion');
                break;
            case 'ERROR':
                echo('ERror al procesar la transaccion');
                break;
            case 'REVIEW':
                echo('Revisar la transaccion');
                break;
            default:
                echo('Respuesta invalida');
                break;
        }
